<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    //Relación muchos a uno
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }

    //Relaciones uno a uno polimórfica
    public function image(){
        return $this->morphOne(Image::class, "imageable");
    }

    //Relación uno a muchos polimrófica

    public function comments(){
        return $this->morphMany(Comment::class,"comentable");
    }

    //Relación muchos a muchos polimórfica

    public function tags(){
        return $this->morphToMany(Tag::class, "taggable");
    }
}
