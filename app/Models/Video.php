<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }

    //Relación uno a muchos polimrófica

    public function comments(){
        return $this->morphMany(Comment::class,"comentable");
    }

    //Relación muchos a muchos polimórfica

    public function tags(){
        return $this->morphToMany(Tag::class, "taggable");
    }
}
